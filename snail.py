def snail(snail_map):
    
    n = len(snail_map[0])
    dir = [(0, 1), (1, 0), (0, -1), (-1, 0)] # (dr, dc) in clockwise order
    visited = set()
    result = []
    
    curr_dir = 0
    r, c = 0, -1
    count = 0
    while count < n**2:
        # peek ahead
        new_r, new_c = r + dir[curr_dir][0], c + dir[curr_dir][1]
        if 0 <= new_r < n and 0 <= new_c < n and (new_r, new_c) not in visited:
            r, c = new_r, new_c
            result.append(snail_map[r][c])
            visited.add((r, c))
            count += 1
        else:
            # turn clockwise
            curr_dir = (curr_dir + 1) % len(dir)
            
    return result       